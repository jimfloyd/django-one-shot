from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)
