from django.db import models

# from django.db.models import CASCADE
from django.conf import settings


# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class TodoItem(models.Model):
    task = models.CharField(max_length=100, default="n/a")
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        "TodoList",
        related_name="items",
        on_delete=models.CASCADE,
    )

# define the structe of the data you want to store. you define it with your models. You use the attributes of task, due date, etc through defining the fields .
# after you create a form, form can use the info in models to infer the kind of interface you would want in front of a user. The model already defined the information and the form can create an interface for the user (put text box, drop down, etc)
# models hide the database from the programmer. db.sqlite3 is the database which model talks to. We dont need to write any sql to the database.
